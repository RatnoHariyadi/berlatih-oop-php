
<?php


require 'animal.php';
require 'Ape.php';
require 'Frog.php';

$sheep=new Animal("Shaun");

echo $sheep->getName(); //shaun
echo "<br>";
echo $sheep->getLeg(); //2
echo "<br>";
echo $sheep->get_cold_blooded(); //false
echo "<br>";
echo "<br>";

$sungokong=new Ape("kera sakti");
echo $sungokong->getName(); //kera sakti
echo "<br>";
echo $sungokong->getLeg(); //2
echo "<br>";
echo $sungokong->get_cold_blooded(); //false
echo "<br>";
echo $sungokong->yell(); // "Auooo"
echo "<br>";
echo "<br>";

$kodok=new Frog("buduk");
echo $kodok->getName(); //kera sakti
echo "<br>";
echo $kodok->getLeg(); //4
echo "<br>";
echo $kodok->get_cold_blooded(); //false
echo "<br>";
echo $kodok->jump(); // "Auooo"
echo "<br>";
echo "<br>";

?>

